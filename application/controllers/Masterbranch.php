<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterbranch extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function index() { 
        if(@$this->session->userdata(isBranchLoggedIn)) {
            redirect('branchdetails');
        }
        else {
            $this->load->view('branch/index');
        }        
    }
    public function branchLogin(){
        if(@$this->session->userdata(isBranchLoggedIn)) {
           redirect('branchdetails');    
        }
        else {
            if($this->input->post('username')){                   
                $where= array(
                    'username'=>$this->input->post('username'),
                    'password' =>$this->input->post('password'),
                    'isactive' => '0'
                );                        
                $checkLogin = $this->Adminmodel->login($where,'branch_login');
                if($checkLogin){
                    $this->session->set_userdata('isBranchLoggedIn',TRUE);
                    $this->session->set_userdata('branchId',$checkLogin[0]['username']);
                    $this->session->set_userdata('branchCode',$checkLogin[0]['branch_code']);
                    $this->session->set_userdata('branch_vendorCode',$checkLogin[0]['vendor_code']);
                    redirect('branchdetails');
                } else{                            
                    $this->session->set_flashdata('error_mesg','<div class="alert alert-danger">Wrong email or password, please try again.</div>');
                }
            } 
            $this->load->view('branch/index');
        }
    }
    //Logout Functionality 
    public function logout(){
        $this->session->unset_userdata('isBranchLoggedIn');
        $this->session->unset_userdata('branchId');
        $this->session->unset_userdata('branchCode');
        $this->session->unset_userdata('branch_vendorCode');
        redirect("Masterbranch");
    }

}

