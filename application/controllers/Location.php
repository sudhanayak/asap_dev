<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Location extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function index() {
        self::viewLocations();
      } 

     public function addLocations(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
            $dataBefore =[];
            $location_name =$this->input->post('location_name');
            $this->load->library('upload');       
            $resultCountry = $this->Adminmodel->getMasterCategory('countries');
            $dataBefore['resultCnt'] = $resultCountry;
            $country_id = $this->input->post('country_id');
            $state_id = $this->input->post('state_id');
            $district_id = $this->input->post('district_id');
            $city_id = $this->input->post('city_id');
            $pincode_id = $this->input->post('pincode_id');
            $location_name = $this->input->post('location_name');
            if(!empty($location_name)){
                $check_data = array(
                "country_id" => $country_id,
                "state_id" => $state_id,
                "district_id"=>$district_id,
                "city_id" => $city_id,
                "pincode_id" => $pincode_id,
                "location_name" => $location_name
                );
                $tablename = "locations";
                $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
                if($checkData > 0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger">Location name already exist</div>') ;
                    $this->load->view('admin/add_location',$dataBefore);
                }else{
                    $admin = $this->session->userdata('userCode');
                    $added_by = $admin!='' ? $admin:'admin' ; 
                    $dateCurrent= date("Y-m-d H:i:s");
                    $country_id = $this->input->post('country_id') =="" ? "":$this->input->post('country_id');
                    $state_id = $this->input->post('state_id') =="" ? "":$this->input->post('state_id');
                    $district_id = $this->input->post('district_id') =="" ? "":$this->input->post('district_id');
                    $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
                    $pincode_id = $this->input->post('pincode_id') =="" ? "":$this->input->post('pincode_id');
                    $dataCity = array(
                        'country_id'=> $country_id,
                        'state_id'=> $state_id,
                        'district_id'=>$district_id,
                        'city_id'=> $city_id,
                        'pincode_id'=> $pincode_id,
                        'location_name'=> $location_name,
                        'created_by'     => $admin ,
                        'created_at'     => $dateCurrent,
                        'updated_at'     => $dateCurrent,
                        'updated_by'     => $added_by
                    );
                    $tableCity="locations";
                    $result = $this->Adminmodel->insertRecordQueryList($tableCity,$dataCity);
                    if($result){
                        $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Location Added Successfully</div>') ;
                       }
                    else{
                        $this->session->set_flashdata('msg','<div class="alert alert-danger">opp! not inserted</div>') ;
                        }           
                        redirect('viewLocations');
                }  
            }
            else{
                 /*$this->session->set_flashdata('msg','<div class="alert alert-danger">fail</div>') ;*/
                   $this->load->view('admin/add_location',$dataBefore);   
            }
            }
    public function viewLocations(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
                        
        $table ="locations";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Location/viewLocations";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'location_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'location_name');
                if($result){
                    foreach ($result as $key => $field) {
                        $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','countries');
                        $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','states');
                         $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','districts');
                        $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','cities');
                        $result[$key]['pincode'] = $this->Adminmodel->getSingleColumnName($field['pincode_id'],'id','pincode','pincodes');
                    } 
                    $data['result'] = $result;
                }else {
                    $result[] = [] ;
                    $data['result'] = $result ;
                }
                $data['searchVal'] = $search !='null'?$search:"";
                $this->load->view('admin/view_locations',$data);
            
            }
    
            // Edit  City 
    public function editLocation(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "locations";
        $tablename2 = "countries";
        $start =0;
        $limit =100;
        $data['countryList']= $this->Adminmodel->get_current_page_records($tablename2,$limit,$start,$column=null,$value=null);
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
            foreach ($result as $key => $field) {
                $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','countries');
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','states');
                 $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','districts');
                $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','cities');
                $result[$key]['pincode'] = $this->Adminmodel->getSingleColumnName($field['pincode_id'],'id','pincode','pincodes');
            }
        $data['result'] = $result[0];
        if($result) {
       $this->load->view('admin/edit_location',$data);
        } else {
         $url='viewLocations';
        redirect($url);
    }  
    }
     public function updateLocation(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $country_id = $this->input->post('country_id');
        $state_id = $this->input->post('state_id');
        $district_id = $this->input->post('district_id');
        $city_id = $this->input->post('city_id');
        $pincode_id = $this->input->post('pincode_id');
        $location_name = $this->input->post('location_name');
        if($location_name!=''){
            $check_data = array(
            "country_id" => $country_id,
            "state_id" => $state_id,
            "district_id"=>$district_id,
            "city_id" => $city_id,
            "pincode_id" => $pincode_id,
            "location_name" => $location_name,
            "id !=" =>$id   
            );
            $tablename = "locations";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">Location name already exist</div>') ;
                $url='location/editLocation/'.$id;
                redirect($url);
            }else{
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;          
            $date     = date("Y-m-d H:i:s");
            $id =$this->input->post('id');
            $dataSubcat = array(
            "country_id" => $country_id,
            "state_id" => $state_id,
            "district_id"=>$district_id,
            "city_id" => $city_id,
            "pincode_id" => $pincode_id,
            "location_name" => $location_name,
            "updated_at" => $date,
            "updated_by" => $added_by,
            );
            $table="locations";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            if($result){
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Location Updated Successfully</div>');
            } else{
                     $this->session->set_flashdata('msg','<div class="alert alert-danger">Opps Some error</div>') ;
            } 
            redirect('viewLocations');
        } 
            $url='location/editLocation/'.$id;
            redirect($url);
        }
        else
        {   
            $url='location/editLocation/'.$id;
            redirect($url);    
        }
    }
        function locationEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'0'
            );
            $table="locations";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='location/viewLocations';
            redirect($url);
        }      
        function locationDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'1'
            );
            $table="locations";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='location/viewLocations';
            redirect($url);
        }
        public function location(){
            $id =$this->input->post('id');
            $result = $this->Adminmodel->getAjaxdata('pincode_id',$id,'locations');
            $data['resultLocation'] =$result;
            $this->load->view('admin/locationAjax',$data);
       }
        function deletelocation($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'locations');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
       
}
?>