<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . '/libraries/REST_Controller.php');

/**
 * Description of RestPostController
 * 
 */
class AsapService  extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->helper("sms");
        $this->load->model('Adminmodel');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
       
    }

     /*
    Register   servive 
    parameters--> fname,lname,username,email,mobile,password,gender
    method--> post
    
    */
    
    public function registerUser_post(){
        $input         = json_decode(file_get_contents('php://input'), true); 
        if(!empty($input['mobile']) && !empty($input['device_type']) ){
                         $mobile        = $input['mobile'];
			$email        =  $input['email'];
			$name        =   $input['name'];			
			$regType = $input['device_type'];
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$input['mobile']) || strlen($input['mobile'])!='10'){
                $responseList = self::success_response('FALSE','6',MESSAGE_INVALDMOBILE);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }elseif($regType !='2' && $regType !='3'){
                $responseList = self::success_response('FALSE','11',MESSAGE_INVALDEMAIL);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
            else{
                $userCheck = array(
                'user_mobile' => $mobile
           	 );
                $checkData = $this->Adminmodel->userExist($userCheck) ;
                if($checkData > 0){
                    $responseList = self::success_response('FALSE','4',MESSAGE_MOBILEEXIST);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
                }else{               
                    
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESSREG);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        } 
    }
    /*
    Login servive 
    parameters--> username , password
    method-->post
    */
   public function loginUser_get(){       
        $mobile = $this->input->get('mobile', TRUE); 
        $password = $this->input->get('password', TRUE); 
        if(isset($mobile) && !empty($password) ){
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$mobile) || strlen($mobile)!='10') {
                $responseList = self::success_response('FALSE','6',MESSAGE_INVALDMOBILE);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }else{
                $data = array(
                'user_mobile'   => $mobile,
                'user_password' => encryptPassword($password),
                );               
                $result = $this->Adminmodel->userLogin($data,$mobile,unserialize(USERCOLUMNLIST));
                if($result)
                {
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result[0]);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
                else
                {
                    $responseList = self::success_response('FALSE','1',MESSAGE_INVALIDCREDENTIALS);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }  
            }
        }else{           
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }
    public function fogetPassword_get(){
        $email = $this->input->get('email', TRUE); 
        $result = $this->Adminmodel->forgetPwd($email);
        if($result){          
            $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESSPWD,$result[0]);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
        else{
            $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
    }
	//User profile 
	public function userProfile_get(){  
        $user_code = $this->input->get('userId', TRUE);
		$user_token = $this->input->get('user_token', TRUE);
		if(!empty($user_code) && !empty($user_token)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);
                       
			if($checkToken > 0){				
				$result = $this->Adminmodel->getUsers($user_code,$user_token);
                                
				if($result){
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result[0]);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
				}
			}else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    }
	//update  profile 
	public function updateProfile_put(){
        $input = json_decode(file_get_contents('php://input'), true);  
        $user_code = $input['userId'];
		$user_token = $input['user_token'];
		if(!empty($user_code) && !empty($user_token) && !empty($input['name']) && !empty($input['email'])){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);
			if($checkToken > 0){
                $name   = $input['name'];
				$email = $input['email'];
				$data = array(
					'user_name'=>$name,
					'user_email'=>$email
				);
				$where = array(
					'user_code'=>$user_code,
					'user_token'=>$user_token
				);
				
				$result = $this->Adminmodel->updateUsers($data,$where);
				if($result){
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result[0]);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
				}
			}else{
                $responseList = self::success_response('FALSE','10',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    }
    // insert query
    public function getUserDtail($id){
        $this->db->where('user_mobile',$id);
        $query = $this->db->get('users');
        $result = $query->result();
        if($result){
            return $result[0];
        }
        else{
            return false ;
        }
    }
    // for chnage password 
    public function changePassword_put(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $userId=$input['userId'];
        $userAuthKey=$input['userAuthKey'];
        if(isset($userId) && isset($userAuthKey) && ($this->Adminmodel->checkUserAuthKey($userId,$userAuthKey) > 0)) {

            @$username     = $input['username'];
            @$old_password = $input['old_password'];
            @$new_password =$input['new_password'];
            @$confirm_password = $input['new_password'];
            if($old_password=='' || $new_password==''){
                $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
                $this->set_response($json_data,REST_Controller::HTTP_OK);  
                
            }
            elseif($new_password!=$confirm_password){
                $responseList = self::success_response('FALSE','8',MESSAGE_PARAMETERMISSING);
                $this->set_response($json_data,REST_Controller::HTTP_OK);   
            }
             elseif($new_password==$old_password){
                $responseList = self::success_response('FALSE','9',MESSAGE_PWDNEWSAME);
                $this->set_response($json_data,REST_Controller::HTTP_OK);  
            }
            else{
                    $data = array(
                        'userId' => $userId ,
                        'user_password' => encryptPassword($old_password),                        
                        );
                    $data1 = array(
                        'userId' => $userId ,
                        'user_password' => encryptPassword($new_password),
                        );    
                    $result = $this->Adminmodel->checkPassword($data,$data1);
                    if($result){
                        $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS);
                        $this->set_response($responseList,REST_Controller::HTTP_OK); 
                    }
                    else{
                        $responseList = self::success_response('TRUE','10',MESSAGE_INVALIDOPWD);
                        $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }                
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
        
    }
   // for send otp 
    public function sendOtp_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        if (isset($input['mobile']) && !empty($input['mobile']) && !empty($input['device_type'])
         && !empty($input['otp_type'])){
            
            $mob="/^[1-9][0-9]*$/";
			$device_type = $input['device_type'];
			$otp_type    = $input['otp_type'];
			
            if(!preg_match($mob,$input['mobile']) || strlen($input['mobile'])!='10') {
                $responseList = self::success_response('FALSE','6',MESSAGE_INVALDMOBILE);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }else{
                    $min = "1230";
                    $max = "5269";
                    $rand1='1234';
                    // update
                    $data1 = array(
                        'user_otp' =>$rand1,                   
                    );
                    // for checking  				
                    $where = array(
                        'otp_mobile' =>$input['mobile'],  
                        'otp_type'   =>$otp_type,  
                        'otp_device_type_id'=>$device_type 					
                    );
                    // for insert				
                    $data2 = array(
                        'otp_mobile' =>$input['mobile'],
                        'user_otp' =>$rand1, 					
                        'otp_type'   =>$otp_type,  
                        'otp_device_type_id'=>$device_type 					
                    ); 					
                    $message1 = urlencode('OTP from asap is '.$rand1.' . Do not share it with any one.'); // Message text required to deliver on mobile number
                    //$sendSMS = sendMobileSMS($message1,$mobile);
                    $result = $this->Adminmodel->userOtp($where,$data1,$data2);
                    if($result){                                        
                        $responseList = self::success_response('TRUE','0',MESSAGE_OTPSENT);
                        $this->set_response($responseList,REST_Controller::HTTP_OK); 
                    }
                    else{
                        $responseList = self::success_response('TRUE','1',MESSAGE_NORECORD,$result[0]);
                        $this->set_response($responseList,REST_Controller::HTTP_OK); 
                    }
                }  
            }else{
                $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
    }    
    // for verify  otp 
    public function verifyOtp_post(){
	    $input = json_decode(file_get_contents('php://input'), true); 
        $mobile = $input['mobile'];
        $otp = $input['otp'];
        $otp_type = $input['otp_type']; 
        $device_type = $input['device_type'];
		$email = $input['email'];
		$name  = $input['name'];
        $password =$input['password'];
        if (isset($mobile) && !empty($mobile) && isset($otp) && !empty($otp) && !empty($device_type) && !empty($otp_type) && isset($email) && !empty($email) && isset($name) && !empty($name) &&
		isset($password) && !empty($password)){ 
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$mobile) || strlen($mobile)!='10') {
                $responseList = self::success_response('FALSE','6',MESSAGE_INVALDMOBILE);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }else{
               
                $otpCheck= array(
                    'otp_mobile' => $mobile,
                    'user_otp'   => $otp,
		    'otp_type'   => $otp_type,
		    'otp_device_type_id' => $device_type,
                );
                  
                $result = $this->Adminmodel->userVerifyOtp($otpCheck);
                if($result){
							
					$password        = encryptPassword($password);
					$date_join     = date('Y-m-d H:i:s');
					$auth_key      = bin2hex(openssl_random_pseudo_bytes(16));            
					$random1       = rand(102,8596);
					$string2       = str_shuffle('1026358132659');
					$random2       = substr($string2,0,3);
					//$contstr       = "ASAP";
					$userId        = $random1.$random2;
					$regType       = $input['device_type'];            
					$user_login_type_id=1;
					$data = array(
					'user_code'         => $userId,
					'user_token'        => $auth_key,
					'user_name'         => $name ,
					'user_email'        => $email ,             			
					'user_mobile'       => $mobile ,
					'user_password'     => $password,
					'user_createdat'        => $date_join,
					'register_devicetype_id'=>$regType,
					'user_login_type_id'  => $user_login_type_id,
					'user_isactive' => '1',			
					);			
					$checkData = $this->Adminmodel->regUser($data) ;	
                    if($checkData){				    
						$responseList = self::success_response('TRUE','0',MESSAGE_OTPVERIFY);
						$this->set_response($responseList,REST_Controller::HTTP_OK); 
					}else{
						$responseList = self::success_response('FALSE','5',MESSAGE_ERROR);
						$this->set_response($json_data,REST_Controller::HTTP_OK);   
					}
                }
                else{
                    $responseList = self::success_response('TRUE','1',MESSAGE_INVALIDOTP);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                } 
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }  
    public function resetPassword_put(){
        $input = json_decode(file_get_contents('php://input'), true); 
        if(isset($input['mobile'])){
            $mobile   = $input['mobile'];
            $password = $input['password'];
            $userCheck = array(
                'user_mobile' => $mobile
            );
            $checkData = $this->Adminmodel->userExist($userCheck);
            if($checkData >0){
                $data1 = array(
                    'user_password' => encryptPassword($password),
                );    
                $result = $this->Adminmodel->resetPassword($mobile,$data1,unserialize(USERCOLUMNLIST)); 
                if($result){
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result[0]);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }else{
                    $responseList = self::success_response('FALSE','7',MESSAGE_NOMOBILE);
                    $this->set_response($json_data,REST_Controller::HTTP_OK);
                }
            }
            else{
                $responseList = self::success_response('FALSE','5',MESSAGE_ERROR);
                $this->set_response($json_data,REST_Controller::HTTP_OK);   
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }        
    }
    public function categories_get(){       
        $result = $this->Adminmodel->categoryList(unserialize(CATEGORYCOLUMNLIST));
        if($result){
            foreach ($result as $key => $field) {    
                $result[$key]['category_appimage'] = base_url()."uploads/category_appimage/".($field['category_appimage']);
                $result[$key]['category_webimage'] = base_url()."uploads/category_webimage/".($field['category_webimage']);   
            }
            $responseList = self::success_response('SUCCESS','0',MESSAGE_SUCCESS,$result);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
        else{                    
            $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);  
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }
    //  resturant list 
    public function restaurantList_get(){
        //$input = json_decode(file_get_contents('php://input'), true); 
        $userId      = $this->input->get('userId', TRUE);
        $userAuthKey = $this->input->get('user_token', TRUE);
        //$userId ='6129326';
        //$userAuthKey = '11cf8f9df65cfad2ae7c91db0d17cfa8';        
        $lat         = '17.4407862';//$input['lat'];
        $long        = '78.39094';//$input['long'];
        $distance    =  10;
        $chefType    = 1;
        $checkPoint  = 20;// else cond
        $minLimit    = 0;
        $maxLimit    = 100;
        if(isset($lat) && isset($long)) {
           $result2 = $this->Adminmodel->restaurantList($lat,$long,$checkPoint,$minLimit,$maxLimit,$distance);
           //print_r($result2);
           //exit;
           $list = array();
           if($result2){
            $k=0;
            $papularList =array();
            $recomandedList =array() ;
            $topRatedList =array();    
            foreach ($result2 as $key => $field) { 
                    $listdada=$this->Adminmodel->restaurantBranchList($lat,$long,$field['vendor_code'],$distance,1) ; 
                    if($listdada !="0"){                            
                        if(!array_key_exists($listdada[$k]['vendor_code'], $list))
                        {        
                           $ard = array("outlet"=>1);
                           $listdada=array_merge($listdada,$ard);                            
                           array_push($list,$listdada);
                        }                           
                    }
                    $k++;
                         $list[$key]['outlet']  =count(self::countOutlet($lat,$long,$field['vendor_code'],$distance,10));
                        $list[$key]['outletList'] = self::countOutlet($lat,$long,$field['vendor_code'],$distance,10);   
                        $web_logo = $this->Adminmodel->singleColumnData('vendor_code',$field['vendor_code'],'web_logo','vendor');
                        $app_logo =  $this->Adminmodel->singleColumnData('vendor_code',$field['vendor_code'],'app_logo','vendor');
                        $restaurant_name =  $this->Adminmodel->singleColumnData('vendor_code',$field['vendor_code'],'restaurant_name','vendor');
                        $list[$key]['web_logo'] = base_url()."uploads/web_logo/".$web_logo;
                        $list[$key]['app_logo'] = base_url()."uploads/app_logo/".$app_logo; 
                        $list[$key]['web_logo'] = base_url()."uploads/web_logo/".$web_logo;
                        $list[$key]['restaurant_name'] = $restaurant_name; 
                        if($list[$key]['toprated']==1){
                            $topRatedList[] =$list[$key];                           
                            unset($list[$key]);
                        }
                        if($list[$key]['popular']==1){
                            $papularList[] =$list[$key];                           
                            unset($list[$key]);
                        }
                        if($list[$key]['recomanded']==1){
                            $recomandedList[] =$list[$key];
                            unset($list[$key]);
                        }
                        //$list['papularList']   = $papularList;
                        //$list['recomandedList']= $recomandedList;
                       // $list['topRatedList']= $topRatedList;
                     
            } 
               
                // //$mainList = array($papularList);
                // //array_push($mainList,$papularList);
                // array_push($mainList,$topRatedList);
                // array_push($mainList,$recomandedList);
                // //$responseList = self::success_response('SUCCESS','0',MESSAGE_SUCCESS,$list);
                $json_data['status']='SUCCESS';
                $json_data['responseCode']='0';
                $json_data['message']=MESSAGE_SUCCESS;
                $json_data['papularList']=$papularList;
                $json_data['recomandedList']=$recomandedList;
                $json_data['topRatedList']=$topRatedList;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
               
            }
            else{
                $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                $this->set_response($responseList,REST_Controller::HTTP_OK); 
            }
        } else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }           
    }
    
    // for  restaurant itemList 
    
    public function restaurantItemList_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $userId      = $input['userId'];
        $userAuthKey = $input['userAuthKey'];
        $userId ='OMLY_tak954';
        $userAuthKey = 'a8c48a1db836eb0e6b048fd629a0a9f3';        
        $vendorId = $input['vendorId'];
        $branchId = $input['branchId'];
        $catId    = $input['catId'];
        $dishType = $input['dishType'];
        $search   = $input['search'];  
        
        if(isset($userId) && isset($userAuthKey) && ($this->Adminmodel->checkUserAuthKey($userId,$userAuthKey) > 0)  && isset($vendorId) && isset($branchId)) {
           $result = $this->Adminmodel->restaurantItemList($vendorId,$branchId,$catId,$dishType,$search);
          
           if($result){
            
                 foreach ($result as $key => $field) {
                       
                        $result[$key]['productImage'] = base_url().'uploads/productImage/'.$field['productImage'];  
                        $result[$key]['addOnproductList'] =  $this->Adminmodel->productAddon($branchId,$field['productId']); 
                        
                          
                }
                $json_data['status']='Success';
                $json_data['responseCode']=0;
                $json_data['message']="Restaurant Item List Retrive Successfully.";
                $json_data['list']=$result;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }
            else{
                $json_data['status']='Fail';
                $json_data['responseCode']=1;
                $json_data['message']=array("Records not found");
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }
        } else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=4;               
                $json_data['message']="Please enter all the mandatory fields";
                $this->set_response($json_data,REST_Controller::HTTP_OK);
        }           
    }
    
    
    // setting detail 
    public function getSettingDetail($column){
        $this->db->where('id','1');
        $query = $this->db->get('basic_settings');
        $result = $query->row();
        if($result){
            return $result->$column;
        }
        else{
            return "" ;
        }
    }
    
    public function countOutlet($lat,$long,$venid,$distance,$nogrp){
        $all_list = array();
        $qyery= $this->db->query("CALL getBrancheList($lat,$long,$venid,$distance,$nogrp)");
        $num=$qyery->num_rows();        
        if($num > 0){
            $result2 = $qyery->result_array();
            mysqli_next_result($this->db->conn_id);
            foreach ($result2 as $key => $field) {
                if($all_list[$key]['vendor_code'] !=$result2[$key]['vendor_code']){
                    $all_list[] = $result2[$key];
                }
            }    
            return $all_list;
        }
        else{
            $array= array();
            return $array ;
        }
    }
    // for success response json 
    public function success_response($status,$responseCode,$message,$data=null){
        $json_data['status']=$status;
        $json_data['responseCode']=$responseCode;
        $json_data['message']=$message;
        if($data !=""){
            $json_data['list']=$data;
        }
        return $json_data ;
    }
    
}   
?>