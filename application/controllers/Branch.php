<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload'); 
    }
    public function index() {
         self::viewBranch();
    } 

    public function viewBranch(){
        if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $table ="vendor_branch_details";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Branch/viewBranch";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'username');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $vendor_code = $this->session->userdata('vendorCode');
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,'vendor_code',$vendor_code,$search,'username');
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";  
        $this->load->view('vendor/view_branch',$data);
    }  

    public function addBranch(){
        if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $username = $this->input->post('username');       
        if($username!=''){            
            $check_data = array(
            "username" => $this->input->post('username')
            );
            $tablename = "vendor_branch_details";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Branch already exist</div>') ;
                $this->load->view('vendor/add_branch');
            }else{
                $min='1452';
                $max='8569';
                $branch_code =rand($min,$max);
                if ($_FILES['cancelled_cheque']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/cancelled_cheque';
                    $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('cancelled_cheque'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $cancelled_cheque    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Branch Cancelled Cheque uploads</div>') ;
                        redirect('addBranch');
                    }        
                } else {
                    $cancelled_cheque    = "";
                }
                $vendor = $this->session->userdata('vendorCode');
                $added_by = $vendor!='' ? $vendor:'vendor' ;           
                $date     = date("Y-m-d H:i:s");
                $password = $this->input->post('password')=="" ? "":$this->input->post('password');
                $branch_adress = $this->input->post('branch_adress')=="" ? "":$this->input->post('branch_adress');
                $latitude = $this->input->post('latitude')=="" ? "":$this->input->post('latitude');
                $longitude = $this->input->post('longitude')=="" ? "":$this->input->post('longitude');
                $delivery_time = $this->input->post('delivery_time')=="" ? "":$this->input->post('delivery_time');
                $min_order = $this->input->post('min_order')=="" ? "":$this->input->post('min_order');
                $delivery_charges = $this->input->post('delivery_charges')=="" ? "":$this->input->post('delivery_charges');
                $opening_time = $this->input->post('opening_time')=="" ? "":$this->input->post('opening_time');
                $closing_time = $this->input->post('closing_time')=="" ? "":$this->input->post('closing_time');
                $bank_name = $this->input->post('bank_name')=="" ? "":$this->input->post('bank_name');
                $account_number = $this->input->post('account_number')=="" ? "":$this->input->post('account_number');
                $ifsc_code = $this->input->post('ifsc_code')=="" ? "":$this->input->post('ifsc_code');
                $bank_branch_name = $this->input->post('bank_branch_name')=="" ? "":$this->input->post('bank_branch_name');
                $bank_address = $this->input->post('bank_address')=="" ? "":$this->input->post('bank_address');
                //FOR Branch Login
                $BranchData = array(
                    'branch_code'=> $branch_code ,
                    'vendor_code'=> $vendor ,
                    'username' => $username,
                    'password' => $password,
                );
                $result = $this->Adminmodel->insertRecordQueryList('branch_login',$BranchData);
                $data = array(
                    'vendor_code'=> $vendor ,
                    'branch_code'=> $branch_code ,
                    'username'=> $username ,                    
                    'password'=>  $password,
                    'branch_adress'=>  $branch_adress,
                    'latitude'=>  '17.4407862',
                    'longitude'=>  '78.39094',
                    'delivery_time'=>  $delivery_time,
                    'min_order'=>  $min_order,
                    'delivery_charges'=>  $delivery_charges,
                    'opening_time'=>  $opening_time,
                    'closing_time'=>  $closing_time,
                    'bank_name'=>  $bank_name,
                    'account_number'=>  $account_number,
                    'ifsc_code'   =>  $ifsc_code,
                    'bank_branch_name'   =>  $bank_branch_name,
                    'bank_address'=> $bank_address ,
                    'cancelled_cheque'=> $cancelled_cheque ,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="vendor_branch_details";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Branch Inserted</div>');
                    redirect('viewBranch');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Branch not inserted</div>') ;
                    redirect('viewBranch');
                } 
            }
        }else {
            $this->load->view('vendor/add_branch');    
        }       
    }
    
    public function editBranch(){
        if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('vendorLogin');
        }
        $tablename = "vendor_branch_details";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
         $data['result'] = $result[0];
        if($result) {
            foreach ($result as $key => $field) {
                $data['cancelled_cheque'] = base_url()."uploads/cancelled_cheque/".$field['cancelled_cheque'];
            }
            $this->load->view('vendor/edit_branch',$data);
        } else {
            $url='viewBranch';
            redirect($url);
        }
    }

    public function updateBranch(){
        if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $id = $this->input->post('id');
        $branch_code = $this->input->post('branch_code');
        if(empty($id)){
            redirect('vendorLogin');
        }
        $username = $this->input->post('username');       
        if($username!=''){            
            $check_data = array(
                "username" => $username,
                "id !=" =>$id   
            );
            $tablename = "vendor_branch_details";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Branch  already exist</div>') ;
            }else{
                if($_FILES['cancelled_cheque']['size'] > 0){
                    $config_media['upload_path'] = './uploads/cancelled_cheque';
                    $config_media['allowed_types'] = 'gif|jpg|jpeg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need                 
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('cancelled_cheque')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $cancelled_cheque    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        $url='Branch/editBranch/'.$id;
                        redirect($url);
                    }        
                } else {
                    $cancelled_cheque    = "";
                }
                $vendor = $this->session->userdata('vendorCode');
                $added_by = $vendor!='' ? $vendor:'vendor';           
                $date     = date("Y-m-d H:i:s"); 
                $password = $this->input->post('password')=="" ? "":$this->input->post('password');
                $branch_adress = $this->input->post('branch_adress')=="" ? "":$this->input->post('branch_adress');
                $latitude = $this->input->post('latitude')=="" ? "":$this->input->post('latitude');
                $longitude = $this->input->post('longitude')=="" ? "":$this->input->post('longitude');
                $delivery_time = $this->input->post('delivery_time')=="" ? "":$this->input->post('delivery_time');
                $min_order = $this->input->post('min_order')=="" ? "":$this->input->post('min_order');
                $delivery_charges = $this->input->post('delivery_charges')=="" ? "":$this->input->post('delivery_charges');
                $opening_time = $this->input->post('opening_time')=="" ? "":$this->input->post('opening_time');
                $closing_time = $this->input->post('closing_time')=="" ? "":$this->input->post('closing_time');
                $bank_name = $this->input->post('bank_name')=="" ? "":$this->input->post('bank_name');
                $account_number = $this->input->post('account_number')=="" ? "":$this->input->post('account_number');
                $ifsc_code = $this->input->post('ifsc_code')=="" ? "":$this->input->post('ifsc_code');
                $bank_branch_name = $this->input->post('bank_branch_name')=="" ? "":$this->input->post('bank_branch_name');
                $bank_address = $this->input->post('bank_address')=="" ? "":$this->input->post('bank_address');
                //FOR Branch Login
                $BranchData = array(
                    'branch_code'=> $branch_code ,
                    'vendor_code'=> $vendor ,
                    'username' => $username,
                    'password' => $password,
                );
                $data = array(
                    'vendor_code'=> $vendor ,
                    'branch_code'=> $branch_code ,                    
                    'username'       =>  $username,
                    'password'=>  $password,
                    'branch_adress'=>  $branch_adress,
                    'latitude'=>  '17.4407862',
                    'longitude'=>  '78.39094',
                    'delivery_time'=>  $delivery_time,
                    'min_order'=>  $min_order,
                    'delivery_charges'=>  $delivery_charges,
                    'opening_time'=>  $opening_time,
                    'closing_time'=>  $closing_time,
                    'bank_name'=>  $bank_name,
                    'account_number'=>  $account_number,
                    'ifsc_code'   =>  $ifsc_code,
                    'bank_branch_name'   =>  $bank_branch_name,
                    'bank_address'=> $bank_address ,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                if($cancelled_cheque!=""){
                    $imgArray =array('cancelled_cheque'=> $cancelled_cheque);
                    $data= array_merge($data,$imgArray);
                }
                $table="vendor_branch_details";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Branch Updated.</div>');
                }else{
                    $url='Branch/editBranch/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Branch not updated.</div>') ;
                }   
            } 
            $url='viewBranch';
            redirect($url);
        }else {   
            $url='Branch/editBranch/'.$id;
            redirect($url); 
        }
    }
    public function branchDetails(){
        if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
        {
        redirect('Mastervendor');
        }
        $input  = json_decode(file_get_contents('php://input'), true);
        $start=0;
        $perPage = 100;
        $id = $this->uri->segment('3');
        //if($start!="" && $perPage!=""){
        $table="vendor_branch_details";
        if($id !=""){
        @$column = "id";
        @$value  = $id;
        }
        $search ='';
    
        $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
        $result=replace_attr($result1);
        if($result){
            foreach ($result as $key => $field) {
                $data['cancelled_cheque'] = base_url()."uploads/cancelled_cheque/".$field['cancelled_cheque'];
            }
            $data['result'] = $result[0] ;
            $this->load->view('vendor/branch_Details',$data);
        }
        else{
        $url='viewBranch';
        redirect($url);
        }
    }
    function BranchEnable($id) {
        $id=$id;
        $BranchData =array(
            'isactive' =>'0'
        );
        $table="vendor_branch_details";
        $result = $this->Adminmodel->updateRecordQueryList($table,$BranchData,'id',$id);
        $url='viewBranch';
        redirect($url);
    }      
    function BranchDisable($id) {
        $id=$id;
        $BranchData =array(
            'isactive' =>'1'
        );
        $table="vendor_branch_details";
        $result = $this->Adminmodel->updateRecordQueryList($table,$BranchData,'id',$id);
        $url='viewBranch';
        redirect($url);
    }
    function deleteBranch($id) {
        $id=$id;
        $result = $this->Adminmodel->delmultipleImage($id,'vendor_branch_details','cancelled_cheque','','cancelled_cheque','','id');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>