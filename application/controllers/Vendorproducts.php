<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vendorproducts extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->library('upload');
    }
     public function index() {
        self::viewVendorproducts();
      } 
    public function addVendorproducts(){
          if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
         {
         redirect('Mastervendor');
         }
        $dataBefore =[];
        $product_name =$this->input->post('product_name');
        $resultCategory = $this->Adminmodel->getMasterCategory('master_category');
        $dataBefore['resultCnt'] = $resultCategory; 
        $vendor = $this->session->userdata('vendorCode');
        if(!empty($product_name)){
            $master_category_id = $this->input->post('master_category_id');
            $category_id = $this->input->post('category_id');
            $sub_category_id = $this->input->post('sub_category_id');
            $check_data = array(
            "vendor_code" => $vendor,
            "master_category_id" => $master_category_id,
            "category_id" => $category_id,
            "sub_category_id" => $sub_category_id,
            "product_name" => $product_name
            );
            $tablename = "vendor_products";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">Vendor Products already exist</div>') ;
            }else{
                if (isset($_FILES['product_image'])) {
                $config_media['upload_path'] = './uploads/product_image';
                $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('product_image'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $product_image    = $data[0]['upload_image']['file_name'];
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Product Image uploads</div>') ;
                    redirect('addVendorproducts');
                }        
            } else {
                $product_image    = "";
            }
                $added_by = $vendor!='' ? $vendor:'vendor' ;   
                $dateCurrent= date("Y-m-d H:i:s");
                $master_category_id = $this->input->post('master_category_id') =="" ? "":$this->input->post('master_category_id');
                $category_id = $this->input->post('category_id') =="" ? "":$this->input->post('category_id');
                $sub_category_id = $this->input->post('sub_category_id') =="" ? "":$this->input->post('sub_category_id');
                $product_type = $this->input->post('product_type') =="" ? "":$this->input->post('product_type');
                $product_description = $this->input->post('product_description') =="" ? "":$this->input->post('product_description');
                $dataCity = array(
                    'master_category_id'=> $master_category_id,
                    'category_id'=> $category_id,
                    'sub_category_id'=> $sub_category_id,
                    'product_name'=> $product_name,
                    'product_description'=> $product_description,
                    'product_type'=> $product_type,
                    'product_image'=>$product_image,
                    'vendor_code'=>   $vendor,
                    'created_by'     => $added_by ,
                    'created_at'     => $dateCurrent,
                    'updated_at'     => $dateCurrent,
                    'updated_by'     => $added_by
                );
                $tableCity="vendor_products";
                $result = $this->Adminmodel->insertRecordQueryList($tableCity,$dataCity);
                if($result) {
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss"> Vendor Products Added Successfully</div>') ;
                } else {
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss"> Vendor Products not Added</div>') ;
                }
                redirect('viewVendorproducts');
            } 
        }
        else{
                /*$this->session->set_flashdata('msg','<div class="alert alert-danger">fail</div>') ;*/
                $this->load->view('vendor/add_vendorproducts',$dataBefore);   
        }
    }
        public function viewVendorproducts(){
              if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
               {
               redirect('Mastervendor');
               }
            $table ="vendor_products";
            $search = ($this->input->get("search"))? $this->input->get("search") : "null";
           $config = array();
           $config['reuse_query_string'] = true;
           $config["base_url"] = base_url() . "Vendorproducts/viewVendorproducts";
           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
           $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'product_name');//search
           $config["per_page"] = PERPAGE_LIMIT;
           $config["uri_segment"] = 3;
           $config['full_tag_open'] = "<ul class='pagination'>";
           $config['full_tag_close'] = '</ul>';
           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';
           $config['cur_tag_open'] = '<li class="active"><a href="#">';
           $config['cur_tag_close'] = '</a></li>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['first_tag_open'] = '<li>';
           $config['first_tag_close'] = '</li>';
           $config['last_tag_open'] = '<li>';
           $config['last_tag_close'] = '</li>';
           $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';
           $this->pagination->initialize($config);
           $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
           $data["links"] = $this->pagination->create_links();
           $limit =$config["per_page"];
           $start=$page;
           $vendor = $this->session->userdata('vendorCode');
           $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,'vendor_code',$vendor,$search,'product_name');
                    if($result){
                        foreach ($result as $key => $field) {
                            $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_category') ;
                            $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','category') ;
                            $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_category_id'],'id','subcategory_name','subcategory');
                        } 
                        $data['result'] = $result;
                    } else {
                        $result[] = [] ;
                        $data['result'] = $result ;
                    }
                    $data['searchVal'] = $search !='null'?$search:"";
                    $this->load->view('vendor/view_vendorproducts',$data);
                }
  public function editVendorproducts(){
      if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
       {
       redirect('Mastervendor');
       }
      $id = $this->uri->segment('3');
      $resultCategory = $this->Adminmodel->getMasterCategory('master_category');
        $data['resultCnt'] = $resultCategory; 
      if($id==''){
          redirect('vendorLogin');
      }
      $tablename = "vendor_products";
      $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
      if($result) {
          foreach ($result as $key => $field) {
              $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_category') ;
              $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','category') ;
              $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_category_id'],'id','subcategory_name','subcategory');
          }
          $data['result'] = $result[0] ;
          $this->load->view('vendor/edit_vendorproducts',$data);
      } else {
          $url='viewVendorproducts';
          redirect($url);
      }
    }
    public function updateVendorproducts(){
        if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
         {
         redirect('Mastervendor');
         }
        $id = $this->input->post('id');
        $vendor = $this->session->userdata('vendorCode');
        if(empty($id)){
            redirect('vendorLogin');
        }
        $product_name = $this->input->post('product_name');       
        if($product_name!=''){            
            $check_data = array(
                "vendor_code" => $vendor,
                "product_name" => $product_name,
                "id !=" =>$id   
            );
            $tablename = "vendor_products";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Vendor Products already exist</div>') ;
            }else{
              if($_FILES['product_image']['size'] > 0){
                    $config_media['upload_path'] = './uploads/product_image';
                    $config_media['allowed_types'] = 'gif|jpg|jpeg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need                 
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('product_image')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $product_image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        $url='Vendorproducts/editVendorproducts/'.$id;
                        redirect($url);
                    }        
                } else {
                    $product_image    = "";
                }
                $added_by = $vendor!='' ? $vendor:'vendor' ;           
                $date     = date("Y-m-d H:i:s");                
                $master_category_id = $this->input->post('master_category_id')=="" ? "":$this->input->post('master_category_id');
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');
                 $sub_category_id = $this->input->post('sub_category_id') =="" ? "":$this->input->post('sub_category_id');
                $product_type = $this->input->post('product_type') =="" ? "":$this->input->post('product_type');
                $product_description = $this->input->post('product_description') =="" ? "":$this->input->post('product_description');
                $data = array(
                    'master_category_id'=> $master_category_id,
                    'category_id'=> $category_id,
                    'sub_category_id'=> $sub_category_id,
                    'product_name'=> $product_name,
                    'product_description'=> $product_description,
                    'product_type'=> $product_type,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                if($product_image!=""){
                    $imgArray =array('product_image'=> $product_image);
                    $data= array_merge($data,$imgArray);
                }
                $table="vendor_products";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Vendor Products Updated.</div>');
                }else{
                    $url='Vendorproducts/editVendorproducts/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error,Vendor Products not updated.</div>') ;
                }   
            } 
            redirect('viewVendorproducts');
        }else {   
            $url='Vendorproducts/editVendorproducts/'.$id;
            redirect($url); 
        }
    }
            
    
            
    
        function vendorproductsEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'0'
            );
            $table="vendor_products";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Vendorproducts/viewVendorproducts';
            redirect($url);
        }      
        function vendorproductsDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'1'
            );
            $table="vendor_products";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Vendorproducts/viewVendorproducts';
            redirect($url);
        }
       
        function deleteVendorproducts($id) {
        $id=$id;
        $result = $this->Adminmodel->delmultipleImage($id,'vendor_products','product_image','product_image');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>