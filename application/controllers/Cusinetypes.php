<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cusinetypes extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
        self::viewCusinetype();
     } 
         
    public function addCusinetype(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
        redirect('admin');
        }
        $cusine_type_name =$this->input->post('cusine_type_name');
        if($cusine_type_name!=''){            
        $check_data = array(
        "cusine_type_name" => $this->input->post('cusine_type_name')    
        );
        $tablename = "cusine_types";
        $checkData = $this->Adminmodel->existData($check_data,$tablename);
        if($checkData > 0){
            $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Cusine type already exist</div>') ;
            $this->load->view('admin/add_cusinetypes');
        }else{
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;           
            $date     = date("Y-m-d H:i:s");
            $data = array(
                'cusine_type_name'=> $cusine_type_name,
                'created_by'     => $added_by ,
                'created_at'     => $date,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table="cusine_types";
            $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Cusine type Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Cusine type not inserted</div>') ;
                }
                redirect('viewCusinetype');
            }
        }else{
            $this->load->view('admin/add_cusinetypes');
            }
        }
                
    public function viewCusinetype(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="cusine_types";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Cusinetypes/viewCusinetype";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'cusine_type_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'cusine_type_name');
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_cusinetypes',$data);
    }
            
           
    public function editCusinetype(){
       if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "cusine_types";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result[0] ;
        if($result) {
           $this->load->view('admin/edit_cusinetypes',$data);
       } else {
           $url='viewCusinetype';
           redirect($url);
       }

            
    }
    public function updateCusinetype(){
       if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $cusine_type_name = $this->input->post('cusine_type_name');       
        if($cusine_type_name!=''){            
            $check_data = array(
                "cusine_type_name" => $cusine_type_name,
                "id !=" =>$id   
            );
            $tablename = "cusine_types";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Cusine Type already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $id =$this->input->post('id');
                $data = array(
                    'cusine_type_name'=> $cusine_type_name,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="cusine_types";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Cusine Type  Updated.</div>');
                }else{
                    $url='Cusinetypes/editCusinetype/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Cusine Type not updated.</div>') ;
                }   
            } 
            redirect('viewCusinetype');
        }else {   
            $url='Cusinetypes/editCusinetype/'.$id;
            redirect($url); 
        }
        
}
    
        function cusinetypeEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'0'
            );
            $table="cusine_types";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Cusinetypes/viewCusinetype';
            redirect($url);
        }      
        function cusinetypeDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'1'
            );
            $table="cusine_types";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Cusinetypes/viewCusinetype';
            redirect($url);
        }
        function deleteCusinetype($id) {
            $id=$id;
            $result = $this->Adminmodel->delRow($id,'cusine_types');
            $data['result'] = $result;
            redirect($_SERVER['HTTP_REFERER']);
        }
}
?>
