<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload'); 
    }
    public function index() {
        self::viewCategory();
    } 

    public function viewCategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="category";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Category/viewCategory";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'category_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'category_name');
       if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_category') ;
            }
        }
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";  
        $this->load->view('admin/view_category',$data);
    }  

    public function addCategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultCategory = $this->Adminmodel->getMasterCategory('master_category');
        $dataBefore['resultCat'] = $resultCategory;
        $category_name = $this->input->post('category_name');       
        if($category_name!=''){            
            $check_data = array(
            "category_name" => $this->input->post('category_name')
            );
            $tablename = "category";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Category already exist</div>') ;
                $this->load->view('admin/add_category',$dataBefore);
            }else{
                if (isset($_FILES['category_webimage'])) {
                    $config_media['upload_path'] = './uploads/category_webimage';
                    $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('category_webimage'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $category_webimage    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in category webimage uploads</div>') ;
                        redirect('addCategory');
                    }        
                } else {
                    $category_webimage    = "";
                }
                if (isset($_FILES['category_appimage'])) {
                    $config_media1['upload_path'] = './uploads/category_appimage';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('category_appimage'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }       
                    $category_appimage    = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in category webimage uploads</div>') ;
                        redirect('addCategory');
                    }        
                } else {
                    $category_appimage    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $master_category_id = $this->input->post('master_category_id')=="" ? "":$this->input->post('master_category_id');           
                $data = array(
                    'master_category_id'=> $master_category_id ,                    
                    'category_name'       =>  $category_name,
                    'category_webimage'   =>  $category_webimage,
                    'category_appimage'   =>  $category_appimage,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="category";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Category Inserted</div>');
                    redirect('viewCategory');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Category not inserted</div>') ;
                    redirect('viewCategory');
                } 
            }
        }else {
            $this->load->view('admin/add_category',$dataBefore);    
        }       
    }
    
    public function editCategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        $resultCategory = $this->Adminmodel->getMasterCategory('master_category');
        $data['resultCat'] = $resultCategory;
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "category";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_category') ;
            }
            $data['result'] = $result[0] ;
            $this->load->view('admin/edit_category',$data);
        } else {
            $url='viewCategory';
            redirect($url);
        }
    }
    public function updateCategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $category_name = $this->input->post('category_name');       
        if($category_name!=''){            
            $check_data = array(
                "category_name" => $category_name,
                "id !=" =>$id   
            );
            $tablename = "category";
            $checkData = $this->Adminmodel->existData($check_data,$tablename);
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Category  already exist</div>') ;
                $url='Category/editCategory/'.$id;
                redirect($url);
            }else{
                if($_FILES['category_webimage']['size'] > 0){
                    $config_media['upload_path'] = './uploads/category_webimage';
                    $config_media['allowed_types'] = 'gif|jpg|jpeg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need                 
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('category_webimage')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $category_webimage    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        $url='Category/editCategory/'.$id;
                        redirect($url);
                    }        
                } else {
                    $category_webimage    = "";
                }
                if($_FILES['category_appimage']['size'] > 0){
                    $config_media1['upload_path'] = './uploads/category_appimage';
                    $config_media1['allowed_types'] = 'gif|jpg|jpeg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need                 
                    $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('category_appimage')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data1[] = array('upload_image' => $this->upload->data());
                    }       
                    $category_appimage    = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        $url='Category/editCategory/'.$id;
                        redirect($url);
                    }        
                } else {
                    $category_appimage    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");                
                $master_category_id = $this->input->post('master_category_id')=="" ? "":$this->input->post('master_category_id');
                $data = array(
                    'master_category_id'=> $master_category_id ,                    
                    'category_name'       =>  $category_name,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                if($category_webimage!=""){
                    $imgArray =array('category_webimage'=> $category_webimage);
                    $data= array_merge($data,$imgArray);
                }
                if($category_appimage!=""){
                    $imgArray =array('category_appimage'=> $category_appimage);
                    $data= array_merge($data,$imgArray);
                }
                $table="category";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Category Updated.</div>');
                }else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Category not updated.</div>') ;
                }
                redirect('viewCategory');   
            } 
            $url='category/editCategory/'.$id;
            redirect($url);
        }else {   
            $url='category/editCategory/'.$id;
            redirect($url); 
        }
    }
    function catEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'isactive' =>'0'
        );
        $table="category";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewCategory';
            redirect($url);
    }      
    function catDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'isactive' =>'1'
        );
        $table="category";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewCategory';
        redirect($url);
    }
    public function catAjax(){
        $id =$this->input->post('id');
        $result = $this->Adminmodel->getAjaxdata('master_category_id',$id,'category');
        $data['resultSubcat'] =$result;
        $this->load->view('admin/catAjax',$data);
    }
    function deleteCategory($id) {
        $id=$id;
        $result = $this->Adminmodel->delmultipleImage($id,'category','category_webimage','category_appimage','category_webimage','category_appimage','id');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>