<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Basicsettings extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library('upload'); 
    }
     public function index() {
         self::editbasicsettings();
        } 
     public function editbasicsettings(){
            $id = 1;
            if($id==''){
                redirect('adminLogin');
            }
            $tablename = "basic_settings";
            $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
            if($result) {
            foreach ($result as $key => $field) {
                $data['Applogo'] = base_url()."uploads/basic_app_logo/".$field['app_logo'];
                $data['Weblogo'] = base_url()."uploads/basic_web_logo/".$field['web_logo'];
                $data['Favicon'] = base_url()."uploads/fav_icon/".$field['fav_icon'];
            }
            $data['result'] = $result[0];
        } 
            $this->load->view('admin/basic_settings',$data);
        }
        public function updatebasic_settings(){
            $id = $this->input->post('id');
            if(empty($id)){
                redirect('adminLogin');
            }
             $site_title = $this->input->post('site_title');      
             if($site_title!=''){            
                 $check_data = array(
                 "site_title" => $site_title,
                 "id !=" =>$id   
                 );
                 $tablename = "basic_settings";
                 $checkData = $this->Adminmodel->existData($check_data,$tablename);
                 if($checkData > 0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Details already exist</div>') ;
                 }else{
                         if($_FILES['web_logo']['size'] > 0)
                         {

                             $config_media2['upload_path'] = './uploads/basic_web_logo';
                             $config_media2['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                             $config_media2['max_size']  = '1000000000000000'; // whatever you need
                             $this->upload->initialize($config_media2);
                             $error = [];
                             if ( ! $this->upload->do_upload('web_logo'))
                             {
                                 $error[] = array('error_image' => $this->upload->display_errors());    
                             }
                             else
                             {
                                 $data1[] = array('upload_image' => $this->upload->data());
                             }      
                             $web_logo = $data1[0]['upload_image']['file_name'];
                             if(count($error) >0){
                                 $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in web logo uploads</div>') ;
                                 redirect(editbasicsettings);
                             }        
                         } else {
                            $web_logo = "";
                         }
                         if($_FILES['app_logo']['size'] > 0)
                         {
                             $config_media['upload_path'] = './uploads/basic_app_logo';
                             $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                             $config_media['max_size']  = '1000000000000000'; // whatever you need
                             $this->upload->initialize($config_media);
                             $error = [];
                             if ( ! $this->upload->do_upload('app_logo'))
                             {
                                 $error[] = array('error_image' => $this->upload->display_errors());    
                             }
                             else
                             {
                                 $data[] = array('upload_image' => $this->upload->data());
                             }      
                             $app_logo = $data[0]['upload_image']['file_name'];
                             if(count($error) >0){
                                 $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in app logo uploads</div>') ;
                                 redirect(editbasicsettings);
                             }        
                         }else {
                            $app_logo = "";
                         }
                         if($_FILES['fav_icon']['size'] > 0)
                         {
                             $config_media1['upload_path'] = './uploads/fav_icon';
                             $config_media1['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                             $config_media1['max_size']  = '1000000000000000'; // whatever you need
                             $this->upload->initialize($config_media1);
                             $error = [];
                             if ( ! $this->upload->do_upload('fav_icon'))
                             {
                                 $error[] = array('error_image' => $this->upload->display_errors());    
                             }
                             else
                             {
                                 $data2[] = array('upload_image' => $this->upload->data());
                             }      
                             $fav_icon = $data2[0]['upload_image']['file_name'];
                             if(count($error) >0){
                                 $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in fav icon uploads</div>') ;
                                 redirect(editbasicsettings);
                             }        
                         } else {
                            $fav_icon = "";
                         }
                             $admin = $this->session->userdata('userCode');
                             $added_by = $admin!='' ? $admin:'admin' ;          
                             $date     = date("Y-m-d H:i:s");
                             $admin_title = $this->input->post('admin_title')=="" ? "":$this->input->post('admin_title');
                             $support_email = $this->input->post('support_email')=="" ? "":$this->input->post('support_email'); 
                             $contact_phone = $this->input->post('contact_phone')=="" ? "":$this->input->post('contact_phone'); 
                              $enquiry_email = $this->input->post('enquiry_email')=="" ? "":$this->input->post('enquiry_email'); 
                              $order_email = $this->input->post('order_email')=="" ? "":$this->input->post('order_email');
                               $contact_email = $this->input->post('contact_email')=="" ? "":$this->input->post('contact_email'); 
                                $sms_phone_number = $this->input->post('sms_phone_number')=="" ? "":$this->input->post('sms_phone_number');
                             $footer_text = $this->input->post('footer_text')=="" ? "":$this->input->post('footer_text');
                              $address = $this->input->post('address')=="" ? "":$this->input->post('address');
                              $max_distance = $this->input->post('max_distance')=="" ? "":$this->input->post('max_distance');  
                             $id =$this->input->post('id');
                             $data = array(
                                 'site_title'=> $site_title ,
                                 'admin_title'=> $admin_title ,
                                 'support_email'  => $support_email,
                                 'contact_phone'  => $contact_phone,                            
                                 'enquiry_email'  => $enquiry_email,
                                 'order_email'  => $order_email,
                                 'contact_email'  => $contact_email,
                                 'enquiry_email'  => $enquiry_email,
                                 'sms_phone_number'  => $sms_phone_number,
                                 'footer_text'  => $footer_text,
                                 'address'  => $address,
                                 'max_distance'  => $max_distance,
                                 'updated_at'     => $date,
                                 'updated_by'     => $added_by
                             );
                             if($app_logo!=""){
                                 $imgArray =array('app_logo'=> $app_logo);
                                $data= array_merge($data,$imgArray);
                             }
                             if($web_logo!=""){
                                 $imgArray =array('web_logo'=> $web_logo);
                                $data= array_merge($data,$imgArray);
                             }
                             if($fav_icon!=""){
                                 $imgArray =array('fav_icon'=> $fav_icon);
                                $data= array_merge($data,$imgArray);
                             }
                             $table="basic_settings";
                             $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                             if($result){
                                     $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Basic Settings Updated</div>');
                                     $this->load->view('admin/basic_settings',$data);
                             }
                             else{
                                    $url='basicsettings/editbasicsettings/'.$id;
                                     redirect($url);
                                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                             }  
                 } 
                 redirect(editbasicsettings);
             }
             else
             {   
                redirect(editbasicsettings);    
             }

         }
}
?>