<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class District extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
     public function index() {
        self::viewDistrict();
      } 
    public function addDistrict(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $dataBefore =[];
        $district_name =$this->input->post('district_name');
        $this->load->library('upload');       
        $resultCountry = $this->Adminmodel->getMasterCategory('countries');
        $dataBefore['resultCnt'] = $resultCountry; 
        if(!empty($district_name)){
            $country_id = $this->input->post('country_id');
            $state_id = $this->input->post('state_id');
            $district_name = $this->input->post('district_name');
            $check_data = array(
                "country_id" => $country_id,
                "state_id" => $state_id,
                "district_name" => $district_name
            );
            $tablename = "districts";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">District name already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ; 
                $dateCurrent= date("Y-m-d H:i:s");
                $country_id = $this->input->post('country_id') =="" ? "":$this->input->post('country_id');
                $state_id = $this->input->post('state_id') =="" ? "":$this->input->post('state_id');
                $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
                $dataDistrict = array(
                    'country_id'=> $country_id,
                    'state_id'=> $state_id,
                    'district_name'=> $district_name,
                    'created_by'     => $admin ,
                    'created_at'     => $dateCurrent,
                    'updated_at'     => $dateCurrent,
                    'updated_by'     => $added_by
                );
                $tableDistrict="districts";
                $result = $this->Adminmodel->insertRecordQueryList($tableDistrict,$dataDistrict);
                if($result){
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">District Added Successfully</div>') ;
                }else{
                   $this->session->set_flashdata('msg','<div class="alert alert-danger">opp! not inserted</div>') ;
                }           
                redirect('viewDistrict');
            }
        }
        else{
                /*$this->session->set_flashdata('msg','<div class="alert alert-danger">fail</div>') ;*/
                $this->load->view('admin/add_district',$dataBefore);   
        }
    }
     public function viewDistrict(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }

        $table ="districts";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "District/viewDistrict";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'district_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'district_name');
                if($result){
                    foreach ($result as $key => $field) {
                        $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','countries');
                        $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','states');
                    } 
                    $data['result'] = $result;
                } else {
                    $result[] = [] ;
                    $data['result'] = $result ;
                }
                $data['searchVal'] = $search !='null'?$search:"";
                $this->load->view('admin/view_district',$data);
            }
            
        // Edit District
    public function editDistrict(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        
            $id = $this->uri->segment('3');
            if($id==''){
                redirect('adminLogin');
            }
            $tablename = "districts";
            $tablename2 = "countries";
            $start =0;
            $limit =100;
            $data['country']= $this->Adminmodel->get_current_page_records($tablename2,$limit,$start,$column=null,$value=null);
            $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
                foreach ($result as $key => $field) {
                    $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','countries');
                    $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','states');
                }
            $data['result'] = $result[0] ;
            if($result) {
                $this->load->view('admin/edit_district',$data);
            } else{
                $url='viewDistrict';
                redirect($url);
            }
        }
    public function updateDistrict(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
            $id = $this->input->post('id');
            if(empty($id)){
                redirect('adminLogin');
            }
             $country_id = $this->input->post('country_id');
             $state_id = $this->input->post('state_id');
             $district_name = $this->input->post('district_name');
             $city_id = $this->input->post('city_id');
             if($district_name!=''){
                 $check_data = array(
                 "country_id" => $country_id,
                 "state_id" => $state_id,
                 "district_name" => $district_name,
                 "id !=" =>$id   
                 );
                 $tablename = "districts";
                 $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

                 if($checkData > 0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger">District name already exist</div>') ;
                 }else{
                     $admin = $this->session->userdata('userCode');
                     $added_by = $admin!='' ? $admin:'admin' ;          
                     $date     = date("Y-m-d H:i:s");
                     $id =$this->input->post('id');
                     $dataSubcat = array(
                         "country_id" => $country_id,
                         "state_id" => $state_id,
                         "district_name" => $district_name,
                         "updated_at" => $date,
                         "updated_by" => $added_by,
                     );
                     $table="districts";
                     $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
                     if($result){
                             $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">District Updated</div>');
                            
                     }
                     else{
                             $this->session->set_flashdata('msg','<div class="alert alert-danger">Opps Some error</div>') ;
                     } 
                     redirect('viewDistrict');
                 } 
                 $url='District/editDistrict/'.$id;
                 redirect($url);
             }
             else
             {   
                 $url='District/editDistrict/'.$id;
                 redirect($url);    
             }

        }
        function districtEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'0'
            );
            $table="districts";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='District/viewDistrict';
            redirect($url);
        }      
        function districtDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'1'
            );
            $table="districts";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='District/viewDistrict';
            redirect($url);
        }
        public function District(){
            $id =$this->input->post('id');
            $result = $this->Adminmodel->getAjaxdata('state_id',$id,'districts');
            $data['resultDistrict'] =$result;
            $this->load->view('admin/districtAjax',$data);
       }
       function deleteDistrict($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'districts');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
       
}
?>