<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class State extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
     public function index() {
        self::viewstate();
       } 
    public function add_state(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
         // to fetch the data fro country drop down
            $data['admin']="add_state";
            $table2 ="countries";
            $start =0;
            $limit =100;
            $data['country']= $this->Adminmodel->get_current_page_records($table2,$limit,$start,$column=null,$value=null);
        //******    
            $state_name = $this->input->post('state_name');     
            if($state_name!=''){            
            $check_data = array(
            "state_name" => $this->input->post('state_name')
            );
            $tablename = "states";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">State already exist</div>') ;
                $this->load->view('admin/'.$data['admin'],$data);
            }else{
                        $admin = $this->session->userdata('userCode');
                        $added_by = $admin!='' ? $admin:'admin' ;           
                        $date     = date("Y-m-d H:i:s");
                        $state_code = $this->input->post('state_code')=="" ? "":$this->input->post('state_code');  
                        $country_id = $this->input->post('country_id')=="" ? "":$this->input->post('country_id');
                        $dataCat = array(
                            'state_name'=> $state_name ,
                            'created_by'     => $added_by ,
                            'created_at'     => $date,
                            'updated_at'     => $date,
                            'updated_by'     => $added_by,
                            'country_id'     => $country_id
                        );
                        $table="states";
                        $result = $this->Adminmodel->insertRecordQueryList($table,$dataCat);
                        if($result){
                                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">State Added</div>');
                        }
                        else{
                                $this->session->set_flashdata('msg','<div class="alert alert-danger">opp! not inserted</div>') ;
                        }           
                        redirect('viewstate');
                    }
            }else
            {
                //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
                $this->load->view('admin/'.$data['admin'],$data);   
            }       
        }
        public function viewstate(){
            if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
            {
              redirect('admin');
            }
            
           $table ="states";
           $search = ($this->input->get("search"))? $this->input->get("search") : "null";
           $config = array();
           $config['reuse_query_string'] = true;
           $config["base_url"] = base_url() . "State/viewstate";
           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
           $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'state_name');//search
           $config["per_page"] = PERPAGE_LIMIT;
           $config["uri_segment"] = 3;
           $config['full_tag_open'] = "<ul class='pagination'>";
           $config['full_tag_close'] = '</ul>';
           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';
           $config['cur_tag_open'] = '<li class="active"><a href="#">';
           $config['cur_tag_close'] = '</a></li>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['first_tag_open'] = '<li>';
           $config['first_tag_close'] = '</li>';
           $config['last_tag_open'] = '<li>';
           $config['last_tag_close'] = '</li>';
           $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';
           $this->pagination->initialize($config);
           $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
           $data["links"] = $this->pagination->create_links();
           $limit =$config["per_page"];
           $start=$page;
           $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'state_name');
                if($result){
                    foreach ($result as $key => $field) {
                        $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','countries');
                    } 
                    $data['result'] = $result;
                } else {
                    $result[] = [] ;
                    $data['result'] = $result ;
                }
                $data['searchVal'] = $search !='null'?$search:"";
                $this->load->view('admin/view_state',$data);
            }
        //edit State
        public function editstate(){
            if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
            {
              redirect('admin');
            }
            $id = $this->uri->segment('3');
            if($id==''){
                redirect('adminLogin');
            }
            $tablename = "states";
            $tablename2 = "countries";
            $start =0;
            $limit =100;
            $data['country']= $this->Adminmodel->get_current_page_records($tablename2,$limit,$start,$column=null,$value=null);
            $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
                foreach ($result as $key => $field) {
                    $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','countries');
                }
            $data['result'] = $result[0];
            if($result) {
                $this->load->view('admin/edit_state',$data); 
            } else {
                $url='viewstate';
                redirect($url);
            }
              
        }
            public function updatestate(){
                if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
                {
                  redirect('admin');
                }
            $id = $this->input->post('id');
            if(empty($id)){
                redirect('adminLogin');
            }
             $state_name = $this->input->post('state_name');
             $country_id = $this->input->post('country_id');
             if($state_name!=''){            
                 $check_data = array(
                 "state_name" => $state_name,
                 "country_id" => $country_id,
                 "id !=" =>$id   
                 );   
                 $tablename = "states";
                 $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

                 if($checkData > 0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger">State name already exist</div>') ;
                 }else{
                     $admin = $this->session->userdata('userCode');
                     $added_by = $admin!='' ? $admin:'admin' ;          
                     $date     = date("Y-m-d H:i:s");
                     $state_code = $this->input->post('state_code')=="" ? "":$this->input->post('state_code'); 
                     $id =$this->input->post('id');
                     $dataSubcat = array(
                         'state_name'=> $state_name ,
                         'country_id'  => $country_id, 
                         'updated_at'     => $date,
                         'updated_by'     => $added_by
                     );
                     $table="states";
                     $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
                     if($result){
                             $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">State Updated</div>');
                     }
                     else{
                             $this->session->set_flashdata('msg','<div class="alert alert-danger">Opps Some error</div>') ;
                     }  
                     redirect('viewstate');
                 } 
                 $url='state/editstate/'.$id;
                 redirect($url);
             }
             else
             {   
                 $url='state/editstate/'.$id;
                 redirect($url);    
             }

        }
         function stateEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'0'
            );
            $table="states";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
             $url='state/viewstate';
            redirect($url);
        }
        function stateDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'1'
            );
            $table="states";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
           $url='state/viewstate';
            redirect($url);
        }
        public function state(){
            $id =$this->input->post('id');
            $result = $this->Adminmodel->getAjaxdata('country_id',$id,'states');
            $data['resultState'] =$result;
            $this->load->view('admin/stateAjax',$data);
        }
         public function stateCode(){
            $id =$this->input->post('id');
            $stateCode = $this->Adminmodel->getSingleColumnName($id,'id','state_code','states');
            $data['stateCode'] =$stateCode;
            $this->load->view('admin/stateCode',$data);
        }
        function deletestate($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'states');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

}
?>