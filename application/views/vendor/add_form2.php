<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Add Form</h4>
						
						<form class="mb-0">
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputEmail4" class="bmd-label-floating">Email</label>
								<input type="email" class="form-control" id="inputEmail4" >
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputEmail4" class="bmd-label-floating">Name</label>
								<input type="name" class="form-control" id="inputEmail4" >
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputEmail4" class="bmd-label-floating">Address</label>
								<textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder=""></textarea>
								</div>
							</div>														
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputState" class="bmd-label-floating">State</label>
								<select id="inputState" class="form-control mb-3 custom-select">
								    <option >select State</option>
									<option>Gujarat</option>
									<option>Kashmir</option>
									<option>Himachal</option>
									<option>Panjab</option>
								</select>
								</div>								
							</div>	
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputState" class="bmd-label-floating">Time</label>
								<div class="input-group clockpicker">
									<input type="text" class="form-control" value="10:10">
									<div class="input-group-append">
										<span class="input-group-text"><i class="fa fa-clock-o"></i></span>
									</div>
								</div>
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputState">Date</label>
								<input type="text" class="form-control" placeholder="2017-06-04" id="mdate">
								
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputState">Multiple select</label><br/>
								<select id="inputState" class="select2 form-control mb-3 custom-select" multiple='true'>
								    <option >select State</option>
									<option>Gujarat</option>
									<option>Kashmir</option>
									<option>Himachal</option>
									<option>Panjab</option>
								</select>
								
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputState">single  select(with search)</label><br/>
								<select id="inputState" class="select2 form-control mb-3 custom-select" >
								    <option >select State</option>
									<option>Gujarat</option>
									<option>Kashmir</option>
									<option>Himachal</option>
									<option>Panjab</option>
								</select>
								
								</div>								
							</div>
							
                            <div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputEmail4" class="m-b-30">detail</label><br/>
								<textarea  class="form-control summernote " maxlength="225" rows="3" placeholder=""></textarea>
								</div>
								
							</div>							
							<button type="submit" class="btn btn-raised btn-primary mb-0">Sign in</button>
						</form>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       