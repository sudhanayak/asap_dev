<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class='row'>  
            <div class="col-md-12 col-xl-12">
                <div class="card m-b-30 m-t-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Edit Product Price</h4>                 
                        <form action= "<?php echo base_url() ?>Productprice/updateProductprice" method="POST" enctype="multipart/form-data" class="mb-0">
                        <?php //echo $this->session->flashdata('msg'); ?>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="Product" class="bmd-label-floating">Product</label>
                                    <select id="ProductId" class="form-control mb-3 custom-select" name="product_id" required>
                                        <option value="<?php echo $result['product_id']; ?>"><?php echo $result['product']; ?> </option>
                                        <option>Select Product</option>
                                        <?php                    
                                            $count = count(array_filter($resultProduct));
                                            if($count > 0) {
                                            $i=0;
                                            foreach($resultProduct as $key => $row){
                                            ?>
                                                <option value="<?php echo  $row['id'] ?>"><?php echo  $row['product_name'] ?></option>
                                            <?php
                                            }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="text-box">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="Subcategory" class="bmd-label-floating">Product Weight</label>
                                        <select id="productWeightId" class="form-control mb-3 custom-select" name="product_weight_id" required>
                                            <option value="<?php echo $result['product_weight_id']; ?>"><?php echo $result['proweight']; ?> </option>  
                                            <option>Select Product Weight</option>
                                    </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="Productprice" class="bmd-label-floating">Product Price</label>
                                        <input type="number" class="form-control" name="product_price" value="<?php echo $result['product_price']; ?>" required>
                                    </div>
                                    <!-- <div class="form-group col-md-4">
                                        <button type="button" id="addWeights" class="btn btn-raised btn-primary mb-0">Add More</button>
                                    </div> -->
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="id" value="<?php echo $result['id']; ?>">
                            <button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
                                
    </div>
</div>
<?php
include_once'footer.php';
?>
                                       