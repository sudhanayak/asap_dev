	</div> <!-- content -->

                <footer class="footer">
                    © 2018 Asap
                </footer>
            </div>
            <!-- End Right content here -->
        </div>
       <!-- jQuery  -->
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/tinymce.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/summernote-updated.min.js"></script>
		<script>
      $(document).ready(function() {
        $('.summernote').summernote();
        $("#ProductId").change(function(){
          id =$(this).val();
          $.ajax({
            url: '<?php echo base_url() ?>Productprice/proweight',
            type: 'POST',
            data: 'id='+id,
            dataType: 'html',
            success: function(response) {
                //alert(response);
              $("#productWeightId").html(response);
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(textStatus, errorThrown);
            }
          });
        });
        var count =0
        $(document).on("click", "#addWeights", function() {                
          count++;
          var product_weight_id ="product_weight_id"+count;
          var product_price  ="product_price"+count;
          var dataPppend ="";                        
          dataPppend='<div class="form-row">';
          dataPppend+='<div class="form-group col-md-4">';
          dataPppend+='<label for="Subcategory" class="bmd-label-floating">Product Weight</label>';
          dataPppend+='<select id="productWeightId" class="form-control mb-3 custom-select" name="product_weight_id[]" required>';
          dataPppend+='<option>Select Product Weight</option></select></div>';
          dataPppend+='<div class="form-group col-md-4">';
          dataPppend+='<label for="Productprice" class="bmd-label-floating">Product Price</label>';
          dataPppend+='<input type="text" class="form-control" name="product_price[]">';
          dataPppend+='</div><div class="form-group col-md-4">';                        
          dataPppend+='<a style="cursor:pointer" class="btn btn-primary removeImg" data-dismiss="fileinput">Remove</a>';
          dataPppend+='</div></div>';
          $(".text-box").append(dataPppend);
        });
        $(document).on("click", ".removeImg", function() {
          $(this).parent().parent().hide();
        });
        setTimeout(function(){ $('.updateSuss').hide(); }, 3000);
      });
    </script>
        <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap-material-design.js"></script>
        <script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/detect.js"></script>
        <script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>

        <!-- Plugins js -->
        <script src="<?php echo base_url() ?>assets/plugins/timepicker/moment.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/timepicker/tempusdominus-bootstrap-4.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/timepicker/bootstrap-material-datetimepicker.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/clockpicker/jquery-clockpicker.min.js"></script>       
        <script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
   
        <!-- Plugins Init js -->
        <script src="<?php echo base_url() ?>assets/pages/form-advanced.js"></script>
		 <!--Wysiwig js-->
        
       
        <!-- App js -->
        <script src="<?php echo base_url() ?>assets/js/app.js"></script>
    </body>
</html>