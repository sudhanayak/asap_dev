<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30">
		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="red" id="wizard">
							<form class="mb-0" action="<?php echo base_url() ?>Vendor/updateVendor" 
                            method="POST" enctype="multipart/form-data">
		                <!--        You can switch " data-color="blue" "  with one of the next bright colors: "green", "orange", "red", "purple"             -->
								<!-- <?php
									echo $this->session->flashdata('msg');
								?> -->
		                    	<div class="wizard-header">
		                        	<h3 class="wizard-title m-l-30">
		                        		Edit vendor
		                        	</h3>
									
		                    	</div>
								<div class="wizard-navigation">
									<ul>
			                            <li><a href="#details" data-toggle="tab">Basic information</a></li>
			                            <li><a href="#captain" data-toggle="tab">Business Information</a></li>
			                            <li><a href="#description" data-toggle="tab">Picture Detail</a></li>
			                        </ul>
								</div>

		                        <div class="tab-content">
		                            <div class="tab-pane" id="details">
									
									
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Name</label>
												<input type="text" name='name' class="form-control" value="<?php echo $result['name']; ?>" required >
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Email</label>
												<input type="email" name='email' class="form-control" value="<?php echo $result['email']; ?>" >
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Mobile</label>
												<input type="number" maxlength="10" minlength="10" name='mobile' class="form-control" required value="<?php echo $result['mobile']; ?>" >
											</div>
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Password</label>
												<input type="password" name='password' class="form-control" required value="<?php echo $result['password']; ?>" >
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">PanCard</label>
												<input type="text" name='vendor_pan_number' class="form-control" value="<?php echo $result['vendor_pan_number']; ?>">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Pancard Image</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
													<div>
														<span class="btn btn-info btn-file">
														<span class="fileinput-new">Select image</span>
														<span class="fileinput-exists">Change</span>
														<input name='vendor_pan_image' type="file">
														</span>
													</div>
												</div>
												
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Aadhar Card</label>
												<input type="text" name='vendor_adhar_number' class="form-control" value="<?php echo $result['vendor_adhar_number']; ?>">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Aadhar Card Image</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
													<div>
													<span class="btn btn-info btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input name='vendor_adhar_image' type="file" name="image">
													</span>

													</div>
												</div>												
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Address</label>
												<textarea id="textarea"  name='address' class="form-control" maxlength="225" rows="3" placeholder=""><?php echo $result['address']; ?></textarea>
											</div>
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Tags</label>
												<input type="text" name='tag_detail' class="form-control" value="<?php echo $result['tag_detail']; ?>">
												<small> for search related keywords</small>
											</div>
											
										</div>
										
										
		                            </div>
		                            <div class="tab-pane" id="captain">
		                                <div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Business Name</label>
												<input type="text" name='business_name' class="form-control" value="<?php echo $result['business_name']; ?>">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Restaurant Name</label>
												<input type="text" name='restaurant_name' class="form-control" value="<?php echo $result['restaurant_name']; ?>">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Category</label>
												<select id="inputState" class="select2 form-control mb-3 custom-select" name="category_id[]" multiple='true'>
													<?php                    
														$count = count(array_filter($resultCat));
														if($count > 0) {
														$i=0;
														foreach($resultCat as $key => $row){
														?>
															<option value="<?php echo  $row['id'] ?>"><?php echo  $row['category_name'] ?></option>
														<?php
														}
														}
													?>
												</select>
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Cusine Type</label>
												<select id="inputState" class="select2 form-control mb-3 custom-select" name="cusine_type_id[]" multiple='true'>
													<?php
														$count = count(array_filter($resultCusine));
														if($count > 0) {
														$i=0;
														foreach($resultCusine as $key => $row){
														?>
															<option value="<?php echo  $row['id'] ?>"><?php echo  $row['cusine_type_name'] ?></option>
														<?php
														}
														}
													?>
												</select>
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Business Pan Card</label>
												<input type="text" name='business_pan_number' class="form-control" value="<?php echo $result['business_pan_number']; ?>">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Business Pan Card Image</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
													<div>
													<span class="btn btn-info btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input type="file" name="business_pan_mage">
													</span>

													</div>
												</div>
												
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Registration Certificate Number</label>
												<input type="text" name='registration_certificate_no' class="form-control" value="<?php echo $result['registration_certificate_no']; ?>">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Registration Certificate Image</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
													<div>
													<span class="btn btn-info btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input type="file" name="registration_certificate_image">
													</span>

													</div>
												</div>
												
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Established Year</label>
												<select name='established_year' id="yearest" class="form-control mb-3 custom-select">
													<option value="<?php echo  $result['established_year'] ?>"><?php echo  $result['established_year'] ?></option>
													<?php
													$now=date('Y');
													for($i=$now;$i>1950;$i--){
													?>
													<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
													<?php
													}
													?>
												</select>
											</div>
											<div class="form-group col-md-6">
												
												<div class="checkbox">
													<label>
													<input type="checkbox" name='make_it_popular'><span class="checkbox-decorator" <?php if($result['make_it_popular'] == 0) { echo 'checked = "checked"'; } ?>><span class="check"></span><div class="ripple-container"></div></span> 	make_it_popular
													</label>
												</div>
											</div>
										</div>
		                                
		                            </div>
		                            <div class="tab-pane" id="description">
									    <div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Web Logo</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
													<div>
													<span class="btn btn-info btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input type="file" name="web_logo">
													</span>

													</div>
												</div>
												
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">App Logo</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
													<div>
													<span class="btn btn-info btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input type="file" name="app_logo">
													</span>

													</div>
												</div>
												
											</div>
										</div>
		                                <div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Web Restaurant Image</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
													<div>
													<span class="btn btn-info btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input type="file" name="web_restaurant_image">
													</span>

													</div>
												</div>
												
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">App Restaurant Image</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
													<div>
													<span class="btn btn-info btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input type="file" name="app_restaurant_image">
													</span>

													</div>
												</div>
												
											</div>
										</div>
										
		                            </div>
		                        </div>
	                        	<div class="wizard-footer">
                                    <input type='hidden' name='id' value='<?php echo $result['id'];?>' />
                                    <input type='hidden' name='vendor_code' value='<?php echo $result['vendor_code'];?>' />
	                            	<div class="pull-right">
	                                    <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next' value='Next' />
	                                    <input type='submit' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish' value='Finish' />
	                                </div>
	                                <div class="pull-left">
	                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />

										
	                                </div>
	                                <div class="clearfix"></div>
	                        	</div>
		                    </form>
		                </div>
		            </div> <!-- wizard container -->
		       </div> <!-- end col -->
		</div> <!-- end row -->
	</div>							
	</div>
</div>
	   <?php
	   include_once'footer.php';
	   ?>
	   <!--     Fonts and icons     -->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
		<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo base_url() ?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
		<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="<?php echo base_url() ?>assets/js/material-bootstrap-wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>